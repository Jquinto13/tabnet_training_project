import numpy as np
from numpy.core.numeric import False_
import pandas as pd
import pickle
import time

def load_pkl_subjects(pkl_subjects_path, split):
    print("Loading Alan splits...")
    with open(pkl_subjects_path, 'rb') as f:
        # Call load method to deserialze
        alan_splits = pickle.load(f)
    if split == 'train':
        subject_names = list(alan_splits['ids_90'])
    elif split == 'test':
        subject_names = list(alan_splits['ids_10'])
    else:
        print("Splits must be one of: {'train', 'test'}")
    print("Alan splits loaded")
    print()
    return subject_names

def load_pkl_data(pkl_fts_path, pkl_subjects_path, gt_columns, split):
    # Load in valid subject names from Alan splits
    subject_names = load_pkl_subjects(pkl_subjects_path, split)
    
    if split == 'train':
        print("Loading pickled training data...")
    elif split == 'test':
        print("Loading pickled testing data...")

    # Load in full pickled container (features + ground truth)
    with open(pkl_fts_path, 'rb') as f:
        full_container = pickle.load(f)

    if split == 'train':
        print("Pickled training data loaded")
    elif split == 'test':
        print("Pickled testing data loaded")
    print()

    print("Creating features dataframe...")
    try:
        features_df = full_container['features']['0-30']
        features_df = features_df.loc[features_df.index.isin(subject_names)]
    except: 
        if split == 'train':
            features_df = full_container['ids_90']['features']['0-30']
        elif split == 'test':
            features_df = full_container['ids_10']['features']['0-30']
        else:
            print("Argument 'split' must be one of: {'train', 'test'}")
            return
    print("Features dataframe created")
    print()

    print("Creating ground truth dataframe...")
    try:
        gt_df = full_container['ground_truth']['0-30']
        gt_df = gt_df.loc[gt_df.index.isin(subject_names), gt_columns] # Need to add PP=3rd column; MAP excluded
    except: 
        if split == 'train':
            gt_df = full_container['ids_90']['ground_truth']['0-30'].loc[:, gt_columns]
        elif split == 'test':
            gt_df = full_container['ids_10']['ground_truth']['0-30'].loc[:, gt_columns]
        else:
            print("Argument 'split' must be one of: {'train', 'test'}")
            return
   
    gt_df['PP'] = gt_df['sysNurse_mean'] - gt_df['diaNurse_mean'] 
    print("Ground truth dataframe created")
    print()
    return features_df, gt_df, subject_names

def get_feature_taxonomy(feat_names_input, taxonomy_path):
    # Load taxonomy table 
    tax = pd.read_excel(taxonomy_path, engine='openpyxl')
    tax = tax.iloc[:518, :13] # Trim to same dimensions as Matlab table; right now this is hard-coded, but we should have a better way of figuring out the correct dimensions
    empty_taxonomy = np.zeros((len(feat_names_input), tax.shape[1]))
    feature_taxonomy = pd.DataFrame(empty_taxonomy, columns=tax.columns.to_list())

    # Determine initials of all input features
    feature_initials_input = [feat_names_input[i].split("_")[0] for i in range(len(feat_names_input))]
    
    # Iterate through taxonomy table and find matching features for each row
    for i in range(tax.shape[0]):
        feat_name_curr = tax.prefix[i]
        
        # Locate underscores and "roi" identifier
        i_underscore_curr = feat_name_curr.find('_')
        i_roi = feat_name_curr.find('_roi')
        if i_roi == -1:
            i_roi = len(feat_name_curr) + 1

        # Get initial for current feature in the taxonomy table
        initial_curr = feat_name_curr[:i_underscore_curr]
        if initial_curr == 'WDAD':
            feat_name_curr_no_initial = feat_name_curr[i_underscore_curr+1:i_roi]
            is_feat = pd.Series(feat_names_input).str.contains(feat_name_curr_no_initial)
        else:
            feat_name_curr_no_roi = feat_name_curr[:i_roi]
            is_feat = pd.Series([initial_curr == i for i in feature_initials_input]) & pd.Series(feat_names_input).str.contains(feat_name_curr_no_roi)

        # Fill all features that match current taxonomy row
        x = np.tile(tax.iloc[i, :], (np.sum(is_feat), 1))
        feature_taxonomy.loc[is_feat, :] = x
        
    return feature_taxonomy

def filter_scheme02_features(feat_names_input, feature_taxonomy):
    """
    Need a way to format this
    """
    valid_fts = (pd.Series(feat_names_input).str.contains('roi21_G') |
    pd.Series(feat_names_input).str.contains('roi21_T4') | 
    pd.Series(feat_names_input).str.contains('roi23_G') | 
    pd.Series(feat_names_input).str.contains('roi23_T4') |
    pd.Series(feat_names_input).str.contains('roi21n23_G') | 
    pd.Series(feat_names_input).str.contains('roi21n23_T4')) & ~pd.Series(feat_names_input).str.contains('Dem') & ~pd.Series(feat_names_input).str.contains('_hr') & ~pd.Series(feat_names_input).str.contains('_hba') & ~pd.Series(feat_names_input).isin(np.array(feat_names_input)[list(feature_taxonomy['class'] == 'Energy')]) & ~pd.Series(feat_names_input).isin(np.array(feat_names_input)[list(feature_taxonomy['class'] == 'GrossACDCNaresh')]) & ~pd.Series(feat_names_input).isin(np.array(feat_names_input)[list(feature_taxonomy['class'] == 'Harmonics')]) & ~pd.Series(feat_names_input).isin(np.array(feat_names_input)[list(feature_taxonomy['class'] == 'HeartRate')]) & ~pd.Series(feat_names_input).isin(np.array(feat_names_input)[list(feature_taxonomy['class'] == 'Histogram')]) & ~pd.Series(feat_names_input).isin(np.array(feat_names_input)[list(feature_taxonomy['class'] == 'Medical')]) & ~pd.Series(feat_names_input).isin(np.array(feat_names_input)[list(feature_taxonomy['class'] == 'Snr')]) & ~pd.Series(feat_names_input).isin(np.array(feat_names_input)[list(feature_taxonomy['class'] == 'SnrMorphology')])

    scheme02_fts = np.array(feat_names_input)[np.array(valid_fts)]
    return scheme02_fts

def filter_demographic_features(feat_names_input):
    valid_dem_fts = pd.Series(feat_names_input).str.contains('Dem') & ~pd.Series(feat_names_input).str.contains('Predicted')
    dem_fts = np.array(feat_names_input)[valid_dem_fts]
    return dem_fts

def load_data(pkl_fts_path, pkl_subjects_path, taxonomy_path, save_path, gt_columns=['diaNurse_mean', 'sysNurse_mean'], split='train', no_med_path=None, no_med=False, include_all_no_med=False):
    features_df, gt_df, subject_names = load_pkl_data(pkl_fts_path, pkl_subjects_path, gt_columns, split)

    # # Temp fix to NaN values in GT Data; will need a way to address these NaNs
    # gt_df['sysNurse_mean'][['AnuraBP-10-INWQCWVTCH_0000008-1', 'AnuraBP-10-SBXKUECKLW_0000111-1', 'AnuraBP-10-SBXKUECKLW_0000265-1']] = [92, 101.5, 128]

    print("Number of subjects from Alan splits: {}".format(len(subject_names)))
    print("Features dataframe shape: {}".format(features_df.shape))
    print("Ground truth dataframe shape: {}".format(gt_df.shape))
    print()

    print("Filtering scheme02 and demographic features...")
    start = time.time()
    feat_names_input = features_df.columns.to_list()
    feature_taxonomy = get_feature_taxonomy(feat_names_input, taxonomy_path) #
    scheme02_fts = filter_scheme02_features(feat_names_input, feature_taxonomy)
    dem_fts = filter_demographic_features(feat_names_input)
    end = time.time()
    print("Time elapsed: {} sec".format(end-start))

    dem_df = features_df.loc[:, dem_fts]
    dem_df['Dem_Gender'] = dem_df['Dem_Gender'].map(lambda x: 1 if x is True else 0)
    features_df = features_df.loc[:, scheme02_fts]

    print("Scheme02 features: {}".format(features_df.shape))
    print("Demographic features: {}".format(dem_df.shape))
    print()

    if no_med is True and no_med_path is not None:
        print("Filtering noMed samples...")
        no_med_df = pd.read_csv(no_med_path)
        if include_all_no_med is True:
            no_med_samples = no_med_df['file_name'].to_list()
        else:
            no_med_samples = no_med_df.loc[no_med_df['BPMedication_1_Yes'] == 0]['file_name'].to_list()
        features_df  = features_df.loc[no_med_samples]
        gt_df = gt_df.loc[no_med_samples]
        dem_df = dem_df.loc[no_med_samples]
        print("noMed samples filtered")
        print("noMed scheme02 features: {}".format(features_df.shape))
        print("noMed demographic features: {}".format(dem_df.shape))
        print("noMed ground truth features: {}".format(gt_df.shape))

    data_dict = {
        "features_train": features_df,
        "ground_truth": gt_df,
        "demography_train": dem_df
    }
    # Save to pickle file
    print("Saving data to {}...".format(save_path))
    with open(save_path, 'wb') as f:
        pickle.dump(data_dict, f)
    print("Data saved to {}".format(save_path))
    print()
    
    return features_df, gt_df, dem_df