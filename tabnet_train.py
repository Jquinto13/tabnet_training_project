from data_loading.data_loading import *
from ft_selection_cuML.ft_selection_cuML import *
from pytorch_tabnet.tab_model import TabNetRegressor

def get_tabnet_fold_model(X, Y1, foldsize, foldnum, model_path, use_captum=False):
    start_time = time.time()

    # nExtra = X.shape[0] % foldsize
    nExtra = 1

    if foldnum == 5:
        X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
        X_CV_val = X[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra]
        Y_CV = Y1.drop(Y1[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
        Y_CV_val = Y1[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra]
    else:
        X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
        X_CV_val = X[(foldnum-1)*foldsize:(foldnum)*foldsize]
        Y_CV = Y1.drop(Y1[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
        Y_CV_val = Y1[(foldnum-1)*foldsize:(foldnum)*foldsize]
    
    print("X_CV SHAPE: ", X_CV.shape)
    print("X_CV_val SHAPE: ", X_CV_val.shape)
    
    # Initialize network
    net = TabNetRegressor()
    
    # Ensure all features are normalized (scaled between 0 and 1)
    scaler = preprocessing.MinMaxScaler()
    scaler.fit(X_CV)
    X_CV_val_scaled = scaler.transform(X_CV_val)
    X_CV_scaled = scaler.transform(X_CV)

    # # Define model save paths
    model_path = "{}_{}.pt".format(model_path, foldnum)

    # Training model
    net.fit(
        X_train=X_CV_scaled,
        y_train=Y_CV[1].values.reshape(-1, 1),
        eval_set=[(X_CV_scaled, Y_CV[1].values.reshape(-1, 1)), (X_CV_val_scaled, Y_CV_val[1].values.reshape(-1, 1))],
        eval_name=['train', 'test'],
        eval_metric=['rmse'],
        max_epochs=100,
        patience=10,
        batch_size=64,
        drop_last=False,
    )
    net.save_model(model_path)

    # First, we can check to see how much of the variance in the training data is captured by the model.
    # We can compute R-squared for this (also referred to as the coefficient of determination).
    preds = net.predict(X_CV_scaled)
    err = mean_squared_error(Y_CV[1], preds.flatten(), squared=False)
    r_squared = r2_score(Y_CV[1], preds.flatten())

    print("Model Training: R-squared for fold {} training data: {}".format(foldnum, r_squared))
    print("Model Training: Mean squared error on fold {} training data: {}".format(foldnum, err))

    preds_val = net.predict(X_CV_val_scaled)
    err_val = mean_squared_error(Y_CV_val[1], preds_val.flatten(), squared=False)
    r_squared_val = r2_score(Y_CV_val[1], preds_val.flatten())

    print("Model Training: R-squared for fold {} validation data: {}".format(foldnum, r_squared_val))
    print("Model Training: Mean squared error on fold {} validation data: {}".format(foldnum, err_val))

    if use_captum is True:
        return net.feature_importances_

    print("Model Training: Elapsed time: {} sec".format(time.time()-start_time))
    print()

    return X_CV_scaled, Y_CV, net, scaler

def train_TabNet(X_train_fs, Y_train, X_test_fs, Y_test, model_path, use_captum=False):
    print("Model Training: Fitting PyTorch Models for target '{}''...".format(fs.target))
    print('Model Training: X_train shape: {}'.format(X_train.shape))
    print('Model Training: y_train shape: {}'.format(Y_train.shape))

    # Shuffle the training set. But seed random order for replicability
    print("Model Training: Shuffling training set...")
    np.random.seed(0)        
    shufId = np.random.permutation(int(len(Y_train)))
    
    X_shuftrainSys = X_train_fs.iloc[shufId]

    Y_shuftrain = Y_train.iloc[shufId]
    X_shuftrainSys = X_shuftrainSys.reset_index(drop=True)
    Y_shuftrain = Y_shuftrain.reset_index(drop=True)
    print("Model Training: Training set shuffled")
    print()

    # Train models on top 300 fts based on CAPTUM values
    print("Model Training: Training PyTorch models on {} features...".format(X_shuftrainSys.shape[1]))
    nParts = X_shuftrainSys.shape[0]

    X_CV1, Y_CV1, net_CV1, scaler1 = get_tabnet_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 1, model_path, use_captum)
    X_CV2, Y_CV2, net_CV2, scaler2 = get_tabnet_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 2, model_path, use_captum)
    X_CV3, Y_CV3, net_CV3, scaler3 = get_tabnet_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 3, model_path, use_captum)
    X_CV4, Y_CV4, net_CV4, scaler4 = get_tabnet_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 4, model_path, use_captum)
    X_CV5, Y_CV5, net_CV5, scaler5 = get_tabnet_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 5, model_path, use_captum)

    print("Model Training: PyTorch models trained on {} features".format(X_shuftrainSys.shape[1]))

    # Transform features to scalars and then predict
    X1 = scaler1.transform(X_test_fs)
    X2 = scaler2.transform(X_test_fs)
    X3 = scaler3.transform(X_test_fs)
    X4 = scaler4.transform(X_test_fs)
    X5 = scaler5.transform(X_test_fs)

    # Since we have 5 models coming out of 5-fold CV, we average their predictions.
    # Get xgb predictions trained on Raw ground truths
    print("Model Training: Averaging XGB model predictions...")

    xgb_preds_av = np.mean([
        net_CV1.predict(X1).flatten(), 
        net_CV2.predict(X2).flatten(), 
        net_CV3.predict(X3).flatten(), 
        net_CV4.predict(X4).flatten(), 
        net_CV5.predict(X5).flatten()
        ], axis=0)

    xgb_preds_df = pd.DataFrame()
    xgb_preds_df[0] = xgb_preds_av
    fs.xgb_preds_df = xgb_preds_df
    print("Model Training: XGB model predictions averaged")
    print()

    print("Model Training Results ({}):".format(fs.target))
    Y_test = Y_test.flatten()
    ErrpristSys = xgb_preds_df[0] - Y_test
    print('Bias: ', ErrpristSys.mean())
    print('SDError: ', ErrpristSys.std())
    print('RMSE: ', mean_squared_error(Y_test, xgb_preds_df[0], squared=False))
    print()

    # Compression Index: SDpred/SDGT
    print("SD ratio of prediction over ground truth: ")
    print ("Systolic BP is {}".format(xgb_preds_df[0].std()/Y_test.std()))
    print()
    print("Correlation between ground truth and prediction: ")
    print ("Systolic BP is {}".format(stats.pearsonr(Y_test, xgb_preds_df[0])))
    print()

    # SDerror/SDGT - Uncertainty remaining
    print("SD error of prediction over ground truth: ")
    print ("Systolic BP is {}".format(ErrpristSys.std()/Y_test.std()))

# Load data following correlation filtering (~1762 features)
with open('/data/intern/john/PKL/step4_fts.pkl', 'rb') as f:
    fs = pickle.load(f)

# Make X_train dataframe (Uncomment this to run Alan features)
X_train = pd.DataFrame(np.concatenate((fs.step2_ft_matrix, fs.ft_matrix_demography), axis=1), columns=list(fs.step2_ft_names) + list(fs.ft_names_demography))
X_train.replace([np.inf, -np.inf], np.nan, inplace=True)
X_train.fillna(X_train.mean(), inplace=True)

# X_train = pd.DataFrame(fs.step4_dem_ft_matrix, columns=fs.step4_dem_ft_names)
y_train = fs.gt_target.reshape(-1, 1)

# Load full testing data (from all 20736 fts -> 1762 after correlation filtering)
PKL_TEST_SAVE_PATH = '/data/intern/john/PKL/test_data_loading32_select_shap.pkl'
with open(PKL_TEST_SAVE_PATH, 'rb') as f:
    test_data = pickle.load(f)

# Combine demography fts w/ main feature matrix
X_test = pd.concat((test_data['demography_train'], test_data['features_train']), axis=1)
X_test = X_test.loc[:, X_train.columns.to_list()].reset_index(drop=True)

assert X_test.columns.equals(X_train.columns)

# Fill NaNs
X_test.fillna(X_train.mean(), inplace=True)
assert X_test.isnull().any().sum() == 0

y_test = test_data['ground_truth']['sysNurse_mean']
assert y_test.isnull().any().sum() == 0

# TabNet expects target variable to be of shape (n, 1)
y_test = np.array(y_test).reshape(-1, 1)

# Load in Alan data
n_dims = 350 
alan_fts = pd.read_csv('/data/intern/john/AD_Elite_GT.list', header=None)
alan_fts = alan_fts.iloc[:, 0].to_list()[:n_dims]
print("Number of Alan Fts: ", len(alan_fts))

# Train TabNet model
model_path = 'Fold_Models_Tabnet/RF_350_Alan/model' # Make sure this is in the format {parent_folder}/{file prefix}
Y_train = pd.DataFrame(fs.full_gt_data)
train_TabNet(
    X_train_fs=X_train.loc[:, alan_fts],
    Y_train=Y_train,
    X_test_fs=X_test.loc[:, alan_fts],
    Y_test=y_test,
    model_path=model_path
)