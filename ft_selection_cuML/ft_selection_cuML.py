import os

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   
os.environ["CUDA_VISIBLE_DEVICES"]="0"

import numpy as np
import pandas as pd
import scipy.stats as stats
import time
import pickle
from xgboost import XGBRegressor
from sklearn.feature_selection import VarianceThreshold

from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, r2_score
from sklearn import preprocessing
from sklearn.feature_selection import VarianceThreshold

import shap
import cupy as cp

# mempool = cp.get_default_memory_pool()
# with cp.cuda.Device(0):
#     mempool.set_limit(size=2*1024**3)  

import copy

# pytorch related imports
# import torch
# import torch.nn as nn
# import torch.optim as optim
# import torch.nn.functional as F
# from torch.optim import lr_scheduler
# from torch.utils.data import Dataset, DataLoader, random_split

# # imports from captum library
# from captum.attr import LayerConductance, LayerActivation, LayerIntegratedGradients
# from captum.attr import IntegratedGradients, DeepLift, GradientShap, NoiseTunnel, FeatureAblation

# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class Feature_Selection:
    """
    Whole pipeline can either use Pandas, or utilize separate ft_names and ft_matrix properties
    """
    def __init__(self, pkl_path, target, float_type='float64', is_alt_dataset=False):
        self.target_dict = {"diastolic": 0, "systolic": 1, "pp": 2}
        if target not in self.target_dict.keys():
            print("Target must be one of {'diastolic','systolic','pp'}")
            return
        self.target = target
        self.finished_ops = {
            'nan_fts': False, 
            'sparse_fts': False, 
            'redundant_fts': False, 
            'demography_fts': False, 
            'ft_ranking': False
            }
        self.step2_ft_names, self.step2_ft_matrix = None, None
        self.step3_ft_names, self.step3_ft_matrix = None, None
        self.step4_ft_names, self.step4_ft_matrix = None, None
        self.step4_dem_ft_names, self.step4_dem_ft_matrix = None, None
        self.step5_ft_names, self.step5_ft_matrix = None, None
        self.load_data(pkl_path, target, float_type, is_alt_dataset)
    
    def load_data(self, pkl_path, target, float_type, is_alt_dataset):
        """
        New way of loading data that doesn't require any matlab containers
        Assumes we have 3 dataframes (features, ground_truth, and demographic features) in a .pkl file generated via 'data_loading.py'
        """
        start_time = time.time()
        print("Step 1: Loading .pkl file...")
        with open(pkl_path, 'rb') as f:
            # Call load method to deserialze
            data_dict = pickle.load(f)
        print("Step 1: .pkl file loaded")

        features_df = data_dict['features_train']
        gt_df = data_dict['ground_truth']
        dem_df = data_dict['demography_train']

        print("Step 1: Features_df {}".format(features_df.shape))
        print("Step 1: GT_df {}".format(gt_df.shape))
        print("Step 1: dem_df {}".format(dem_df.shape))

        self.ft_names = features_df.columns.to_list()
        self.ft_names_demography = dem_df.columns.to_list()
        self.subject_IDs = features_df.index.to_list()

        self.ft_matrix_demography = dem_df.values
        self.ft_matrix = features_df.values
        self.full_gt_data = gt_df.values

        # Option to set float type
        # if is_alt_dataset is False:
        #     if float_type != 'float64':
        self.ft_matrix = self.ft_matrix.astype(float_type)
        self.ft_matrix_demography = self.ft_matrix_demography.astype(float_type)

        # If ground-truth data contains NaNs, then filter those rows/subjects out
        self.gt_target = self.full_gt_data[:, self.target_dict[target]] # Select ground truth columnn (sys, dias, pp...)
        # if is_alt_dataset is False:
        self.gt_target = self.gt_target.astype(float_type)
        print("Step 1: GT_Target shape: ", self.gt_target.shape)
        gt_mask = ~np.isnan(self.gt_target)
        print("Step 1: Null values in GT_Target: ", np.count_nonzero(np.isnan(self.gt_target)))

        self.ft_matrix = self.ft_matrix[gt_mask, :] # filter feature matrix 
        self.ft_matrix_demography = self.ft_matrix_demography[gt_mask, :]  # filter demography matrix
        self.full_gt_data = self.full_gt_data[gt_mask, :] # filter ground-truth matrix
        self.gt_target = self.gt_target[gt_mask]
        self.subject_IDs = np.array(self.subject_IDs)[gt_mask]

        self.df = pd.DataFrame(self.ft_matrix, columns=self.ft_names, index=self.subject_IDs)
        self.step1_ft_names = self.ft_names
        self.step1_ft_matrix = self.ft_matrix
        print("Step 1: Scheme02 features: {}".format(self.df.shape))
        print("Step 1: Elapsed time: {} sec".format(time.time()-start_time))

        print()

    def get_feats_nan_ids(self, nan_thresh=0.015):
        """
        nan_thresh must be from 0-1, inclusive
        """
        if self.finished_ops['nan_fts'] is True:
            print("NaN features already removed")
            return
        print("Step 2: Removing NaN features...")
        print("Step 2: NaN threshold = {}".format(nan_thresh))
        start_time = time.time()
        min_count = int((1 - nan_thresh)*self.df.shape[0] + 1)
        # self.df.replace([np.inf, -np.inf], np.nan, inplace=True)
        self.df.dropna(thresh=min_count, axis=1, inplace=True)
        self.ft_matrix =  np.array(self.df)
        self.ft_names = np.array(self.df.columns.to_list())
        self.finished_ops['nan_fts'] = True

        print("Step 2: NaN features removed")
        print("Step 2: Remaining features = {}".format(self.df.shape))
        print("Step 2: Elapsed time: {} sec".format(time.time()-start_time))
        print()
        self.step2_ft_names = self.ft_names
        self.step2_ft_matrix = self.ft_matrix

        # # Further step: remove subjects that have a certain percentage of NaN features
        # min_subject_count = int((1 - self.nan_thresh)*self.df.shape[1] + 1)
        # self.df.dropna(thresh=min_subject_count, axis=0, inplace=True)

    def get_feats_sparse_and_no_var_ids(self, sparse_thresh=0):
        """
        sparse_thresh must be between 0-1, inclusive
        """
        if self.finished_ops['sparse_fts'] is True:
            print("Sparse features already removed")
            return
        print("Step 3: Removing sparse features...")
        print("Step 3: Sparsity threshold = {}".format(sparse_thresh))
        
        # Start timing
        start_time= time.time()

        # Impute NaNs in each column with the mean of that column (we might consider kNN imputation in the future)
        self.df.fillna(self.df.mean(), inplace=True)

        # Compute Z-Score of each column; if that column has a Z-score sum of 0 or NaN, then remove it
        z_scores = stats.zscore(self.df, ddof=1)
        z_score_sums = np.sum(z_scores, axis=0)
        valid_cols = (z_score_sums != 0) & (~np.isnan(z_score_sums))
        self.df = self.df.loc[:, valid_cols]

        # zdf = pd.DataFrame(z_scores, columns=self.df.columns)
        # zdf.drop([col for col, val in zdf.sum().iteritems() if val == sparse_thresh or np.isnan(val)], axis=1, inplace=True) ## == sparse_thresh or > abs(sparse_thresh)?
        # self.df = self.df.loc[:, zdf.columns.to_list()]

        self.ft_matrix =  self.df.values
        self.ft_names = np.array(self.df.columns.to_list())
        self.finished_ops['sparse_fts'] = True

        print("Step 3: Sparse features removed")
        print("Step 3: Remaining features = {}".format(self.df.shape))
        print("Step 3: Elapsed time: {} sec".format(time.time()-start_time))
        print()
        self.step3_ft_names = self.ft_names
        self.step3_ft_matrix = self.ft_matrix

    def get_feats_sparse_and_no_var_ids_alt(self, sparse_thresh=0):
        """
        sparse_thresh must be between 0-1, inclusive
        """
        if self.finished_ops['sparse_fts'] is True:
            print("Sparse features already removed")
            return
        print("Step 3: Removing sparse features...")
        print("Step 3: Sparsity threshold = {}".format(sparse_thresh))

        start_time = time.time()

        selector = VarianceThreshold(threshold=sparse_thresh)
        selector.fit(self.df)
        valid_fts = self.df.columns[selector.get_support()]
        self.df = self.df.loc[:, valid_fts]
        
        # Impute NaNs in each column with the mean of that column (we might consider kNN imputation in the future)
        self.df.fillna(self.df.mean(), inplace=True)
        assert self.df.isnull().any().sum() == 0
        
        self.ft_matrix = self.df.values
        self.ft_names = np.array(self.df.columns.to_list())
        self.finished_ops['sparse_fts'] = True

        print("Step 3: Sparse features removed")
        print("Step 3: Remaining features = {}".format(self.df.shape))
        print("Step 3: Elapsed time: {} sec".format(time.time()-start_time))
        print()
        self.step3_ft_names = self.ft_names
        self.step3_ft_matrix = self.ft_matrix

    def corr(self, a, b):
        """
        Python version of Matlab's corr() function 
        """
        a = (a - a.mean(axis=0))/a.std(axis=0)
        b = (b - b.mean(axis=0))/b.std(axis=0)
        corr_coef = np.dot(b.T, a)/b.shape[0]
        if isinstance(corr_coef, np.ndarray):
            return corr_coef[0]
        return corr_coef   

    def corr_GPU(self, a, b):
        """
        Python version of Matlab's corr() function, implemented on GPU 
        """
        a = (a - cp.mean(a, axis=0))/cp.std(a, axis=0)
        b = (b - cp.mean(b, axis=0))/cp.std(b, axis=0)
        corr_coef = cp.dot(b.T, a)/b.shape[0]
        if corr_coef.ndim > 1:
            return corr_coef[0]
        return corr_coef    
    
    def remove_redundant_feats(self, corr_thresh=0.7, min_feats_thresh=600): 
        """
        Ported directly from Matlab (~23-27 min)
        """     
        start_time = time.time() 
        if self.finished_ops['redundant_fts'] is True:
            print("Redundant features already removed")
            return

        if np.count_nonzero(np.isnan(self.gt_target)) > 0:
            print("Ground truth data contains NaN values")
            return

        print("Step 4: Removing redundant features...")
        print("Step 4: Correlation threshold = {}".format(corr_thresh))
        print("Step 4: Minimum features threshold = {}".format(min_feats_thresh))

        # Get column-wise correlation 
        corrFeat = self.corr(self.ft_matrix, self.gt_target.reshape(-1, 1))
        absCorrFeat = abs(corrFeat)

        # Sort correlation matrix in descending order
        sortAbsCorrFeat, idxSortFeat = np.sort(absCorrFeat)[::-1], np.argsort(absCorrFeat)[::-1]
        # Sort feature matrix columns in same order as above
        self.ft_matrix = self.ft_matrix[:, idxSortFeat]
        # Sort feature names by same order as above
        self.ft_names = self.ft_names[idxSortFeat]

        idx = list(range(self.ft_matrix.shape[1]))

        for k in range(self.ft_matrix.shape[1]):
            if k % 100 == 0:
                print("k={} ; {} ".format(k, np.array(idx).shape))
            idx_rem = []
            for i in range(k+1, len(idx)):
                if abs(self.corr(self.ft_matrix[:, idx[k]], self.ft_matrix[:, idx[i]])) > corr_thresh: # Could we vectorize this?
                    idx_rem.append(i)
            idx = np.delete(idx, idx_rem)
            if len(idx) <= min_feats_thresh or k > len(idx):
                break
        feat_rem_index = list(idx)
        self.ft_matrix = self.ft_matrix[:, feat_rem_index]
        self.ft_names = self.ft_names[feat_rem_index]
        self.df = pd.DataFrame(self.ft_matrix, columns=self.ft_names, index=self.subject_IDs)
        # self.df = self.df.loc[:, feat_rem_index]
        self.finished_ops['redundant_fts'] = True
        print("Step 4: Redundant features removed")
        print("Step 4: Remaining features = {}".format(self.df.shape))
        print("Step 4: Elapsed time: {} min".format((time.time()-start_time)/60))
        print()
        self.step4_ft_names = self.ft_names
        self.step4_ft_matrix = self.ft_matrix
    
    def remove_redundant_feats_GPU(self, corr_thresh=0.7, min_feats_thresh=600): 
        """
        Uses CuPy library -> ~2 min 
        """     
        start_time = time.time() 
        if self.finished_ops['redundant_fts'] is True:
            print("Redundant features already removed")
            return

        if np.count_nonzero(np.isnan(self.gt_target)) > 0:
            print("Ground truth data contains NaN values")
            return

        print("Step 4: Removing redundant features...")
        print("Step 4: Correlation threshold = {}".format(corr_thresh))
        print("Step 4: Minimum features threshold = {}".format(min_feats_thresh))

        # Convert to cuPy arrays
        self.ft_matrix = cp.array(self.ft_matrix)
        self.gt_target = cp.array(self.gt_target)

        # Get column-wise correlation 
        corrFeat = self.corr_GPU(self.ft_matrix, self.gt_target.reshape(-1, 1))
        absCorrFeat = cp.abs(corrFeat)

        # Sort correlation matrix in descending order
        sortAbsCorrFeat, idxSortFeat = cp.sort(absCorrFeat)[::-1], cp.argsort(absCorrFeat)[::-1]
        # Sort feature matrix columns in same order as above
        self.ft_matrix = self.ft_matrix[:, idxSortFeat]
        # Sort feature names by same order as above
        self.ft_names = self.ft_names[cp.asnumpy(idxSortFeat)]

        del corrFeat
        del absCorrFeat
        del sortAbsCorrFeat
        del idxSortFeat

        for k in range(self.ft_matrix.shape[1]):
            if k % 100 == 0 or k == self.ft_matrix.shape[1]:
                print("k={}".format(k))
                print(self.ft_matrix.shape)
            if self.ft_matrix.shape[1] <= 600 or k >= self.ft_matrix.shape[1]:
                break
            corrs = cp.abs(self.corr_GPU(self.ft_matrix[:, k+1:], self.ft_matrix[:, k].reshape(-1, 1)))
            # print(corrs.shape)
            idx_rem = corrs > 0.7 # bool mask that tells us which features have a corr_coef > 0.7 -> True
            # remove all columns with a correlation_coefficient greater than 0.7
            idx_rem = ~idx_rem
            # print(idx_rem.shape)
            reduced_feat_matrix = self.ft_matrix[:, k+1:][:, idx_rem]
            self.ft_matrix = cp.concatenate((self.ft_matrix[:, :k+1], reduced_feat_matrix), axis=1)
            reduced_feat_names = self.ft_names[k+1:][cp.asnumpy(idx_rem)]
            self.ft_names = np.concatenate((self.ft_names[:k+1], reduced_feat_names), axis=0)
            del corrs
        
        self.ft_matrix = cp.asnumpy(self.ft_matrix)
        self.gt_target = cp.asnumpy(self.gt_target)

        self.df = pd.DataFrame(self.ft_matrix, columns=self.ft_names, index=self.subject_IDs)
        # self.df = self.df.loc[:, feat_rem_index]
        self.finished_ops['redundant_fts'] = True
        print("Step 4: Redundant features removed")
        print("Step 4: Remaining features = {}".format(self.df.shape))
        print("Step 4: Elapsed time: {} min".format((time.time()-start_time)/60))
        print()
        self.step4_ft_names = self.ft_names
        self.step4_ft_matrix = self.ft_matrix
        
    def append_demographic_features(self):
        if self.finished_ops['demography_fts'] is True:
            print("Demography features already added")
            return

        print("Step 4_Dem: Appending {} demographic features... ".format(self.ft_matrix_demography.shape[1]))
        self.ft_names = np.concatenate((self.ft_names_demography, self.ft_names), axis=0)
        self.ft_matrix = np.concatenate((self.ft_matrix_demography, self.ft_matrix), axis=1)
        self.df = pd.DataFrame(self.ft_matrix, columns=self.ft_names, index=self.subject_IDs)
        self.finished_ops['demography_fts'] = True
        print("Step 4_Dem: Remaining Features = {}".format(self.ft_matrix.shape))
        print()
        self.step4_dem_ft_names = self.ft_names
        self.step4_dem_ft_matrix = self.ft_matrix

    def get_RF_model(self, X1, Y1, n_est, n_jobs, use_xgb):
        """
        Taken directly from Naresh's 'rand_forests.ipynb'
        Trains a RandomForest model.
        100 is the defaults for n_estimators. 
        But for feature selection we need to exhaust the space as much as possible.
        """
        start_time = time.time()
        # if use_xgb is True:
        #     # rf_CV = XGBRFRegressor(n_estimators=n_est, gpu_id=1, tree_method='gpu_hist', num_parallel_tree=n_est)
        # else:
        rf_CV = RandomForestRegressor(n_estimators=n_est, n_jobs=n_jobs)
        
        # Ensure all features are normalized (scaled between 0 and 1)
        scaler = preprocessing.MinMaxScaler()
        scaler.fit(X1)
        X_CV = scaler.transform(X1)
        
        # Now, train the 5 models with all the predictors only using the training set splits
        rf_CV.fit(X_CV, Y1[self.target_dict[self.target]])
        
        # First, we can check to see how much of the variance in the training data is captured by the model.
        # We can compute R-squared for this (also referred to as the coefficient of determination).
        print("Step 5: R-squared for each subsample's training data: ", rf_CV.score(X_CV, Y1[1]))
        print("Step 5: Mean squared error on the training set: ", mean_squared_error(Y1[1], rf_CV.predict(X_CV)))
        print("Step 5: Elapsed time: {} min".format((time.time()-start_time)/60))
        
        return X_CV, Y1, rf_CV, scaler
        
    def get_feature_importances(self, save_path, n_est=10000, n_jobs=12, use_xgb=False):
        if self.finished_ops['ft_ranking'] is True or self.finished_ops['demography_fts'] is False:
            print("Feature ranking already complete or demography features missing")
            return
        print("Step 5: Obtaining feature importances for target '{}''...".format(self.target))
        # Get features and ground truths
        Y_trainpre = self.full_gt_data
        X_trainSys = self.ft_matrix
        
        # Convert to dataframes
        X_trainSys = pd.DataFrame(X_trainSys)
        Y_train = pd.DataFrame(Y_trainpre)

        # Shuffle X and Y
        np.random.seed(0)
        shufId = np.random.permutation(int(len(Y_train)))
        X_shuftrainSys = X_trainSys.iloc[shufId]
        Y_shuftrain = Y_train.iloc[shufId]
        
        X_shuftrainSys = X_shuftrainSys.reset_index(drop=True)
        Y_shuftrain = Y_shuftrain.reset_index(drop=True)
        
        # Run random forests feature selection 
        X_CV1Sys1, Y_CV1, rfSys1CV1, scaler1Sys1 = self.get_RF_model(X_shuftrainSys, Y_shuftrain, n_est, n_jobs, use_xgb)
        print("Step 5: Feature importances calculated")
      
        rfSysImp1 = pd.DataFrame(rfSys1CV1.feature_importances_)
        # explainer = shap.TreeExplainer(rfSys1CV1)
        # shap_values = explainer.shap_values(X_CV1Sys1)

        # print("Model Training: Averaging SHAP values...")
        # shap_mean_vals = np.mean(np.abs(shap_values), axis=0)
        # print("Model Training: SHAP values averaged; shape {}".format(shap_mean_vals.shape))
        
        # Feature importances are ranked in ascending order
        # feature_order = np.argsort(shap_mean_vals)[::-1] # Sorts the 1762 fts after Step4_dem
        rfSysImp1 = pd.DataFrame(rfSysImp1)
        colSys1 = rfSysImp1.sort_values(by=[0], ascending=False)

        colSys = pd.DataFrame()
        colSys[0] = colSys1.index
        self.ft_rankings = list(colSys1.index)

        self.ft_matrix = self.ft_matrix[:, self.ft_rankings]
        self.ft_names = self.ft_names[self.ft_rankings]
        self.df = pd.DataFrame(self.ft_matrix, columns=self.ft_names, index=self.subject_IDs)

        # Give a file name that identifies at least the target (i.e. Systolic in this case), and the date
        print("Step 5: Saving feature importances to {}...".format(save_path))
        colSys.to_csv(save_path)
        print("Step 5: Feature importances saved to {}...".format(save_path))
        self.finished_ops['ft_ranking'] = True
        print()
        self.step5_ft_matrix = self.ft_matrix
        self.step5_ft_names = self.ft_names

        return X_CV1Sys1, Y_CV1, rfSys1CV1, scaler1Sys1

    def get_XGB_fold_Model(self, X, Y1, foldsize, foldnum, n_estimators, max_depth, n_jobs, use_shap=False):
        start_time = time.time()
        # nExtra = X.shape[0] % foldsize
        nExtra = 1
        if foldnum == 5:
            X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
            X_CV_val = X[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra]
            Y_CV = Y1.drop(Y1[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
            Y_CV_val = Y1[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra]
        else:
            X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
            X_CV_val = X[(foldnum-1)*foldsize:(foldnum)*foldsize]
            Y_CV = Y1.drop(Y1[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
            Y_CV_val = Y1[(foldnum-1)*foldsize:(foldnum)*foldsize]
        
        print("X_CV SHAPE: ", X_CV.shape)
        print("X_CV_val SHAPE: ", X_CV_val.shape)

        # Create instances of the RandomForest model. 
        xgb_CV = XGBRegressor(n_estimators=n_estimators, max_depth=max_depth, n_jobs=n_jobs)
        
        # Ensure all features are normalized (scaled between 0 and 1)
        scaler = preprocessing.MinMaxScaler()
        X_CV_scaled = scaler.fit_transform(X_CV)
        X_CV_val_scaled = scaler.transform(X_CV_val)

        xgb_CV.fit(X_CV_scaled, Y_CV[1])
        
        # First, we can check to see how much of the variance in the training data is captured by the model.
        # We can compute R-squared for this (also referred to as the coefficient of determination).
        print("Model Training: R-squared for fold {} training data: {}".format(foldnum, xgb_CV.score(X_CV_scaled, Y_CV[1])))
        print("Model Training: Mean squared error on fold {} training data: {}".format(foldnum, mean_squared_error(Y_CV[1], xgb_CV.predict(X_CV_scaled))))

        print("Model Training: R-squared for fold {} validation data: {}".format(foldnum, xgb_CV.score(X_CV_val_scaled, Y_CV_val[1])))
        print("Model Training: Mean squared error on fold {} validation data: {}".format(foldnum, mean_squared_error(Y_CV_val[1], xgb_CV.predict(X_CV_val_scaled))))
        
        if use_shap is True:
            # Calculate shap values
            explainer = shap.TreeExplainer(xgb_CV)
            shap_values = explainer.shap_values(X_CV_scaled)
            print("Model Training: Elapsed time: {} sec".format(time.time()-start_time))
            print()
            return shap_values

        print("Model Training: Elapsed time: {} sec".format(time.time()-start_time))
        print()
        return X_CV_scaled, Y_CV, xgb_CV, scaler
    
    def load_test_data(self, test_path, n_dims):
        print("Model Training: Loading test data...")
        with open(test_path, 'rb') as f:
            test_data = pickle.load(f)
        print("Model Training: Test data loaded")
        print("Model Training: Test data shape {}".format(test_data['features_train'].shape))

        # Load and process test features
        test_df = pd.concat((test_data['demography_train'], test_data['features_train']), axis=1)
        print("Model Training: Filtering test data using selected features...")
        test_df = test_df.loc[:, self.ft_names]    
        test_df = test_df.iloc[:, :n_dims]
        print("Model Training: Filtered test data shape {}".format(test_df.shape))

        # Load and process test ground truth data
        gt_data = test_data['ground_truth']
        print("Model Training: Ground truth data shape: {}".format(gt_data.shape))

        print("Model Training: Number of ground truth data NaNs: {}".format(gt_data.isnull().any().sum()))
        gt_mask = ~np.isnan(gt_data.iloc[:, self.target_dict[self.target]].values)

        print("Model Training: Filling {} test data NaNs w/ mean of training data...".format(test_df.isnull().any().sum()))
        test_df = test_df.loc[gt_mask, :]
        test_df.fillna(self.df.mean(), inplace=True)
        print("Model Training: Nulls remaining: {}".format(test_df.isnull().any().sum()))

        gt_data = gt_data.loc[gt_mask, :]

        X_test = pd.DataFrame(test_df.values)
        X_test = X_test.T.reset_index(drop=True).T
        y_test = pd.DataFrame(gt_data.values)

        return X_test, y_test

    def get_feature_importances_shap(self, test_path, n_dims=100, n_estimators=300, max_depth=4, n_jobs=-1):
          # if ~np.all(self.finished_ops.values()):
        #     print("Feature selection operations not complete")
        #     return
        # Load in X_train and y_train
        start_time = time.time()
        X_train = self.df
        Y_train = pd.DataFrame(self.full_gt_data)

        print("Model Training: Fitting XGB regressors for target '{}''...".format(self.target))
        print('Model Training: X_train shape: {}'.format(X_train.shape))
        print('Model Training: y_train shape: {}'.format(Y_train.shape))

        # # Reduce to top 100 features - Alan's findings from performance curves
        # print("Model Training: Selecting top {} features...".format(n_dims))
        # X_train = X_train.iloc[:, :n_dims]
        # print("Model Training: X_train shape after selecting top {} features: {}".format(n_dims, X_train.shape))
        
        # Shuffle the training set. But seed random order for replicability
        print("Model Training: Shuffling training set...")
        np.random.seed(0)        
        shufId = np.random.permutation(int(len(Y_train)))
        X_shuftrainSys = X_train.iloc[shufId]
        Y_shuftrain = Y_train.iloc[shufId]
        X_shuftrainSys = X_shuftrainSys.reset_index(drop=True)
        Y_shuftrain = Y_shuftrain.reset_index(drop=True)
        print("Model Training: Training set shuffled")
        print()

        # This part is just to distribute across 5 folds. Can be changed.
        # Right now I am not separating by participant. Only separating by measurement.
        # A cleaner, no contamination shuffle will involve separating by participant.
        nParts = X_shuftrainSys.shape[0] # K-Fold using sklearn?

        print("Model Training: Training XGB models on {} features...".format(X_shuftrainSys.shape[1]))
        shap_vals1 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),1, n_estimators, max_depth, n_jobs, use_shap=True)
        shap_vals2 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),2, n_estimators, max_depth, n_jobs, use_shap=True)
        shap_vals3 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),3, n_estimators, max_depth, n_jobs, use_shap=True)
        shap_vals4 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),4, n_estimators, max_depth, n_jobs, use_shap=True)
        shap_vals5 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),5, n_estimators, max_depth, n_jobs, use_shap=True)
        print("Model Training: XGB models trained on {} features".format(X_shuftrainSys.shape[1]))
        
        print("Model Training: Averaging SHAP values...")
        shap_vals = [shap_vals1, shap_vals2, shap_vals3, shap_vals4, shap_vals5]
        shap_mean_vals = []
        for sv in shap_vals:
            shap_mean_vals.append(np.mean(np.abs(sv), axis=0))
        kfold_shap_vals = np.mean(np.array(shap_mean_vals), axis=0)
        print("Model Training: SHAP values averaged; shape {}".format(kfold_shap_vals.shape))
        
        # Feature importances are ranked in ascending order
        feature_order = np.argsort(kfold_shap_vals)[::-1] # Sorts the 1762 fts after Step4_dem

        # Save features
        with open('Data/PKL/shap_feature_order.pkl', 'wb') as f:
            pickle.dump(feature_order, f)

        self.ft_rankings = feature_order
        # ordered_fts = [(self.step4_dem_ft_names)[i] for i in feature_order]

        # Filter to 100 top SHAP features
        print("Model Training: Filtering to top {} features".format(n_dims))
        X_shuftrainSys_shap = X_shuftrainSys.iloc[:, feature_order[:n_dims]]
        print("Model Training: Elapsed time: {} min".format((time.time()-start_time)/60))
        print()

        # Train models on top 100 fts based on SHAP values
        print("Model Training: Training XGB models on {} features...".format(X_shuftrainSys_shap.shape[1]))
        X_CV1, Y_CV1, xgb_CV1, scaler1 = self.get_XGB_fold_Model(X_shuftrainSys_shap,Y_shuftrain,int(nParts/5),1, n_estimators, max_depth, n_jobs, use_shap=False)
        X_CV2, Y_CV2, xgb_CV2, scaler2 = self.get_XGB_fold_Model(X_shuftrainSys_shap,Y_shuftrain,int(nParts/5),2, n_estimators, max_depth, n_jobs, use_shap=False)
        X_CV3, Y_CV3, xgb_CV3, scaler3 = self.get_XGB_fold_Model(X_shuftrainSys_shap,Y_shuftrain,int(nParts/5),3, n_estimators, max_depth, n_jobs, use_shap=False)
        X_CV4, Y_CV4, xgb_CV4, scaler4 = self.get_XGB_fold_Model(X_shuftrainSys_shap,Y_shuftrain,int(nParts/5),4, n_estimators, max_depth, n_jobs, use_shap=False)
        X_CV5, Y_CV5, xgb_CV5, scaler5 = self.get_XGB_fold_Model(X_shuftrainSys_shap,Y_shuftrain,int(nParts/5),5, n_estimators, max_depth, n_jobs, use_shap=False)
        print("Model Training: XGB models trained on {} features".format(X_shuftrainSys_shap.shape[1]))

        # Load full testing data (all 20736 fts -> 1762 after filtering)
        print("Model Training: Loading testing data...")
        with open(test_path, 'rb') as f:
            test_data = pickle.load(f)

        final_fts = self.step4_dem_ft_names[feature_order]
        
        # Combine demography fts w/ main feature matrix
        train_df = pd.DataFrame(np.array(X_shuftrainSys_shap), columns=final_fts[:n_dims])
        test_df = pd.concat((test_data['demography_train'], test_data['features_train']), axis=1)
        test_df = test_df.loc[:, final_fts[:n_dims]].reset_index(drop=True)
        assert test_df.columns.equals(train_df.columns)

        test_df.fillna(train_df.mean(), inplace=True)
        assert test_df.isnull().any().sum() == 0

        gt_test = test_data['ground_truth']

        if gt_test.isnull().any().sum() != 0:
            gt_mask = ~np.isnan(gt_test.iloc[:, self.target_dict[self.target]].values)
            gt_test = gt_test.loc[gt_mask]
            test_df = test_df.loc[gt_mask]

        assert gt_test.isnull().any().sum() == 0

        print("Model Training: X_test shape: {}".format(test_df.shape))
        self.X_test, self.y_test = pd.DataFrame(test_df.values), pd.DataFrame(gt_test.values)
        X_test, y_test = pd.DataFrame(test_df.values), pd.DataFrame(gt_test.values)

        # Transform features to scalars and then predict
        X1 = scaler1.transform(X_test.astype(X_shuftrainSys_shap.values.dtype))
        X2 = scaler2.transform(X_test.astype(X_shuftrainSys_shap.values.dtype))
        X3 = scaler3.transform(X_test.astype(X_shuftrainSys_shap.values.dtype))
        X4 = scaler4.transform(X_test.astype(X_shuftrainSys_shap.values.dtype))
        X5 = scaler5.transform(X_test.astype(X_shuftrainSys_shap.values.dtype))

        # Since we have 5 models coming out of 5-fold CV, we average their predictions.
        # Get xgb predictions trained on Raw ground truths
        print("Model Training: Averaging XGB model predictions...")
        xgb_preds_av = np.mean([
            xgb_CV1.predict(X1), 
            xgb_CV2.predict(X2), 
            xgb_CV3.predict(X3), 
            xgb_CV4.predict(X4), 
            xgb_CV5.predict(X5)
            ], axis=0)
        xgb_preds_df = pd.DataFrame()
        xgb_preds_df[0] = xgb_preds_av
        self.xgb_preds_df = xgb_preds_df
        print("Model Training: XGB model predictions averaged")
        print()

        print("Model Training Results ({}):".format(self.target))
        ErrpristSys = xgb_preds_df[0] - y_test[1]
        print('Bias: ', ErrpristSys.mean())
        print('SDError: ', ErrpristSys.std())
        print('RMSE: ', mean_squared_error(y_test[1], xgb_preds_df[0], squared=False))
        print()
        # Compression Index: SDpred/SDGT
        print("SD ratio of prediction over ground truth: ")
        print ("Systolic BP is {}".format(xgb_preds_df[0].std()/y_test[1].std()))
        print()
        print("Correlation between ground truth and prediction: ")
        print ("Systolic BP is {}".format(stats.pearsonr(y_test[1], xgb_preds_df[0])))
        print()
        # SDerror/SDGT - Uncertainty remaining
        print("SD error of prediction over ground truth: ")
        print ("Systolic BP is {}".format(ErrpristSys.std()/y_test[1].std()))
    
    def train_XGB_model(self, test_path, n_dims=100, n_estimators=300, max_depth=4, n_jobs=-1):
        # if ~np.all(self.finished_ops.values()):
        #     print("Feature selection operations not complete")
        #     return
        # Load in X_train and y_train
        X_train = self.df
        Y_train = pd.DataFrame(self.full_gt_data)

        print("Model Training: Fitting XGB regressors for target '{}''...".format(self.target))
        print('Model Training: X_train shape: {}'.format(X_train.shape))
        print('Model Training: y_train shape: {}'.format(Y_train.shape))

        # Reduce to top 100 features - Alan's findings from performance curves
        print("Model Training: Selecting top {} features...".format(n_dims))
        X_train = X_train.iloc[:, :n_dims]
        print("Model Training: X_train shape after selecting top {} features: {}".format(n_dims, X_train.shape))

        # Shuffle the training set. But seed random order for replicability
        print("Model Training: Shuffling training set...")
        np.random.seed(0)
        shufId = np.random.permutation(int(len(Y_train)))
        X_shuftrainSys = X_train.iloc[shufId]
        Y_shuftrain = Y_train.iloc[shufId]
        X_shuftrainSys = X_shuftrainSys.reset_index(drop=True)
        Y_shuftrain = Y_shuftrain.reset_index(drop=True)
        print("Model Training: Training set shuffled")
        print()

        # This part is just to distribute across 5 folds. Can be changed.
        # Right now I am not separating by participant. Only separating by measurement.
        # A cleaner, no contamination shuffle will involve separating by participant.
        nParts = X_shuftrainSys.shape[0] # K-Fold using sklearn?
        print("Model Training: Training XGB models...")
        X_CV1, Y_CV1, xgb_CV1, scaler1 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),1, n_estimators, max_depth, n_jobs)
        X_CV2, Y_CV2, xgb_CV2, scaler2 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),2, n_estimators, max_depth, n_jobs)
        X_CV3, Y_CV3, xgb_CV3, scaler3 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),3, n_estimators, max_depth, n_jobs)
        X_CV4, Y_CV4, xgb_CV4, scaler4 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),4, n_estimators, max_depth, n_jobs)
        X_CV5, Y_CV5, xgb_CV5, scaler5 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),5, n_estimators, max_depth, n_jobs)
        print("Model Training: XGB models trained")
        X_test, y_test = self.load_test_data(test_path, n_dims)
        self.X_test, self.y_test = X_test, y_test

        # Transform features to scalars and then predict
        X1 = scaler1.transform(X_test)
        X2 = scaler2.transform(X_test)
        X3 = scaler3.transform(X_test)
        X4 = scaler4.transform(X_test)
        X5 = scaler5.transform(X_test)

        # Since we have 5 models coming out of 5-fold CV, we average their predictions.
        # Get xgb predictions trained on Raw ground truths
        print("Model Training: Averaging XGB model predictions...")
        xgb_preds_av = np.mean([
            xgb_CV1.predict(X1), 
            xgb_CV2.predict(X2), 
            xgb_CV3.predict(X3), 
            xgb_CV4.predict(X4), 
            xgb_CV5.predict(X5)
            ], axis=0)
        xgb_preds_df = pd.DataFrame()
        xgb_preds_df[0] = xgb_preds_av
        self.xgb_preds_df = xgb_preds_df
        print("Model Training: XGB model predictions averaged")
        print()

        print("Model Training Results ({}):".format(self.target))
        ErrpristSys = xgb_preds_df[0] - y_test[1]
        print('Bias: ', ErrpristSys.mean())
        print('SDError: ', ErrpristSys.std())
        print('RMSE: ', mean_squared_error(y_test[1], xgb_preds_df[0], squared=False))
        print()
        # Compression Index: SDpred/SDGT
        print("SD ratio of prediction over ground truth: ")
        print ("Systolic BP is {}".format(xgb_preds_df[0].std()/y_test[1].std()))
        print()
        print("Correlation between ground truth and prediction: ")
        print ("Systolic BP is {}".format(stats.pearsonr(y_test[1], xgb_preds_df[0])))
        print()
        # SDerror/SDGT - Uncertainty remaining
        print("SD error of prediction over ground truth: ")
        print ("Systolic BP is {}".format(ErrpristSys.std()/y_test[1].std()))
                    
    def perform_all_FS_ops(self, save_path, test_path, nan_thresh=0.015, sparse_thresh=0, corr_thresh=0.7, min_feats_thresh=600, nest=10000):
        """
        Performs all feature selection operations
        """
        self.get_feats_nan_ids(nan_thresh)
        self.get_feats_sparse_and_no_var_ids(sparse_thresh)
        self.remove_redundant_feats(corr_thresh, min_feats_thresh)
        self.append_demographic_features()
        self.get_feature_importances(save_path, nest)
        self.train_XGB_model(test_path)

    # def train(self, model_inp, train_iter, criterion, num_epochs):
    #     best_model_wts = copy.deepcopy(model_inp.state_dict())

    #     model_inp.train()
    #     optimizer = torch.optim.Adam(model_inp.parameters(), lr=1e-3)

    #     for epoch in range(num_epochs): 
    #         running_loss = 0.0
    #         for inputs, labels in train_iter:
    #             inputs = inputs.to(device)
    #             labels = labels.to(device)
    #             # forward pass
    #             outputs = model_inp(inputs)
    #             # defining loss
    #             loss = criterion(outputs.float(), labels.float())
    #             # zero the parameter gradients
    #             optimizer.zero_grad()
    #             # computing gradients
    #             loss.backward()
    #             # accumulating running loss
    #             running_loss += loss.item()
    #             # updated weights based on computed gradients
    #             optimizer.step()
    #         best_model_wts = copy.deepcopy(model_inp.state_dict())  
    #         if epoch % 20 == 0:
    #             print('Epoch [%d]/[%d] running accumulative loss across all batches: %.3f' %
    #                     (epoch + 1, num_epochs, running_loss/len(train_iter)))
    #         running_loss = 0.0
    #     model_inp.load_state_dict(best_model_wts)

    #     return model_inp

    # def train_model(self, model_obj, train_iter, criterion, model_path, num_epochs):   
    #     model_obj = self.train(model_obj, train_iter, criterion, num_epochs)
    #     print('Finished training the model. Saving the model to the path: {}'.format(model_path))
    #     print()
    #     torch.save(model_obj.state_dict(), model_path)

    #     return model_obj

    # def get_pytorch_fold_model(self, X, Y1, foldsize, foldnum, use_captum, num_epochs):
    #     start_time = time.time()

    #     # Account for left over samples from splitting dataset into 5
    #     nExtra = X.shape[0] % foldsize

    #     if foldnum == 5:
    #         X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
    #         X_CV_val = X[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra]
    #         Y_CV = Y1.drop(Y1[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
    #         Y_CV_val = Y1[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra]
    #     else:
    #         X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
    #         X_CV_val = X[(foldnum-1)*foldsize:(foldnum)*foldsize]
    #         Y_CV = Y1.drop(Y1[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
    #         Y_CV_val = Y1[(foldnum-1)*foldsize:(foldnum)*foldsize]
        
    #     print("X_CV SHAPE: ", X_CV.shape)
    #     print("X_CV_val SHAPE: ", X_CV_val.shape)
        
    #     # Initialize networkl
    #     net = NuraModel(input_shape=X_CV.shape[1])
    #     net = net.to(device)
    #     net.train()
        
    #     # Loss function = MSE
    #     criterion = nn.MSELoss(reduction='mean')
        
    #     # Ensure all features are normalized (scaled between 0 and 1)
    #     scaler = preprocessing.MinMaxScaler()
    #     scaler.fit(X_CV)

    #     X_CV_val_scaled = scaler.transform(X_CV_val)
    #     X_CV_scaled = scaler.transform(X_CV)

    #     # Create dataloader
    #     model_path = "Fold_Models_Captum/model_{}.pt".format(foldnum)
    #     train_ds = Nura_Dataset(X_CV_scaled, Y_CV[1].values.reshape(-1, 1))
    #     train_dl = DataLoader(train_ds, batch_size=32, shuffle=True)

    #     # Train and save model
    #     net = self.train_model(net, train_dl, criterion, model_path, num_epochs=num_epochs)
        
    #     # Get model outputs on training fold
    #     net.eval()
    #     outputs = net(torch.tensor(X_CV_scaled).float().to(device))
    #     outputs_val = net(torch.tensor(X_CV_val_scaled).float().to(device))

    #     # First, we can check to see how much of the variance in the training data is captured by the model.
    #     # We can compute R-squared for this (also referred to as the coefficient of determination).
    #     err = mean_squared_error(Y_CV[1], outputs.detach().cpu().numpy().flatten(), squared=False)
    #     r_squared = r2_score(Y_CV[1], outputs.detach().cpu().numpy().flatten())

    #     print("Model Training: R-squared for fold {} training data: {}".format(foldnum, r_squared))
    #     print("Model Training: Mean squared error on fold {} training data: {}".format(foldnum, err))

    #     err_val = mean_squared_error(Y_CV_val[1], outputs_val.detach().cpu().numpy().flatten(), squared=False)
    #     r_squared_val = r2_score(Y_CV_val[1], outputs_val.detach().cpu().numpy().flatten())

    #     print("Model Training: R-squared for fold {} validation data: {}".format(foldnum, r_squared_val))
    #     print("Model Training: Mean squared error on fold {} validation data: {}".format(foldnum, err_val))

    #     if use_captum is True:
    #         # Compute CAPTUM importance values 
    #         print("Calculating CAPTUM values...")
    #         X_val_tensor = torch.from_numpy(X_CV_val_scaled).type(torch.FloatTensor)
    #         # y_train_tensor = torch.from_numpy(Y_CV[1].values).type(torch.FloatTensor)
    #         start = time.time()
    #         X_val_tensor.requires_grad_()
    #         ig = IntegratedGradients(net.to('cpu'))

    #         attr, delta = ig.attribute(X_val_tensor, n_steps=50, return_convergence_delta=True)
    #         attr_sum = attr.detach().numpy()
    #         importances = np.mean(attr_sum, axis=0)
    #         end = time.time()
    #         print("CAPTUM values obtained; Time elapsed: {} sec".format(end-start))
    #         print(importances.shape)

    #         print("Model Training: Elapsed time: {} sec".format(time.time()-start_time))
    #         print()
    #         return importances

    #     print("Model Training: Elapsed time: {} sec".format(time.time()-start_time))
    #     print()
        return X_CV_scaled, Y_CV, net, scaler
    
    # def get_feature_importances_captum(self, test_path, n_dims=300, num_epochs=200):
    #     # Load in X_train and y_train
    #     start_time = time.time()
    #     X_train = self.df
    #     Y_train = pd.DataFrame(self.full_gt_data)

    #     print("Model Training: Fitting PyTorch Models for target '{}''...".format(self.target))
    #     print('Model Training: X_train shape: {}'.format(X_train.shape))
    #     print('Model Training: y_train shape: {}'.format(Y_train.shape))
        
    #     # Shuffle the training set. But seed random order for replicability
    #     print("Model Training: Shuffling training set...")
    #     np.random.seed(0)        
    #     shufId = np.random.permutation(int(len(Y_train)))
    #     X_shuftrainSys = X_train.iloc[shufId]
    #     Y_shuftrain = Y_train.iloc[shufId]
    #     X_shuftrainSys = X_shuftrainSys.reset_index(drop=True)
    #     Y_shuftrain = Y_shuftrain.reset_index(drop=True)
    #     print("Model Training: Training set shuffled")
    #     print()

    #     # This part is just to distribute across 5 folds. Can be changed.
    #     # Right now I am not separating by participant. Only separating by measurement.
    #     # A cleaner, no contamination shuffle will involve separating by participant.
    #     nParts = X_shuftrainSys.shape[0] # K-Fold using sklearn?

    #     print("Model Training: Training PyTorch models on {} features...".format(X_shuftrainSys.shape[1]))
    #     print()
    #     captum_vals1 = self.get_pytorch_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 1, use_captum=True, num_epochs=num_epochs)
    #     captum_vals2 = self.get_pytorch_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 2, use_captum=True, num_epochs=num_epochs)
    #     captum_vals3 = self.get_pytorch_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 3, use_captum=True, num_epochs=num_epochs)
    #     captum_vals4 = self.get_pytorch_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 4, use_captum=True, num_epochs=num_epochs)
    #     captum_vals5 = self.get_pytorch_fold_model(X_shuftrainSys, Y_shuftrain, int(nParts/5), 5, use_captum=True, num_epochs=num_epochs)
    #     print("Model Training: PyTorch models trained on {} features".format(X_shuftrainSys.shape[1]))

    #     print("Model Training: Averaging CAPTUM values...")
    #     captum_vals = [captum_vals1, captum_vals2, captum_vals3, captum_vals4, captum_vals5]
    #     kfold_captum_vals = np.mean(np.array(captum_vals), axis=0)
    #     print("Model Training: CAPTUM values averaged; shape {}".format(kfold_captum_vals.shape))
        
    #     # Sorts the 1762 fts after Step4_dem in descending order
    #     feature_order = np.argsort(kfold_captum_vals)[::-1] 
    #     self.ft_rankings = feature_order

    #     # Filter to top 300 Captum features
    #     print("Model Training: Filtering to top {} features".format(n_dims))
    #     X_shuftrainSys_captum = X_shuftrainSys.iloc[:, feature_order[:n_dims]]
    #     print("Model Training: Elapsed time: {} min".format((time.time()-start_time)/60))
    #     print()

    #     # Train models on top 300 fts based on SHAP values
    #     print("Model Training: Training XGB models on {} features...".format(X_shuftrainSys_captum.shape[1]))
    #     X_CV1, Y_CV1, net_CV1, scaler1 = self.get_pytorch_fold_model(X_shuftrainSys_captum, Y_shuftrain, int(nParts/5), 1, use_captum=False, num_epochs=num_epochs)
    #     X_CV2, Y_CV2, net_CV2, scaler2 = self.get_pytorch_fold_model(X_shuftrainSys_captum, Y_shuftrain, int(nParts/5), 2, use_captum=False, num_epochs=num_epochs)
    #     X_CV3, Y_CV3, net_CV3, scaler3 = self.get_pytorch_fold_model(X_shuftrainSys_captum, Y_shuftrain, int(nParts/5), 3, use_captum=False, num_epochs=num_epochs)
    #     X_CV4, Y_CV4, net_CV4, scaler4 = self.get_pytorch_fold_model(X_shuftrainSys_captum, Y_shuftrain, int(nParts/5), 4, use_captum=False, num_epochs=num_epochs)
    #     X_CV5, Y_CV5, net_CV5, scaler5 = self.get_pytorch_fold_model(X_shuftrainSys_captum, Y_shuftrain, int(nParts/5), 5, use_captum=False, num_epochs=num_epochs)
    #     print("Model Training: XGB models trained on {} features".format(X_shuftrainSys_captum.shape[1]))

    #     # Load full testing data (all 20736 fts -> 1762 after filtering)
    #     print("Model Training: Loading testing data...")
    #     with open(test_path, 'rb') as f:
    #         test_data = pickle.load(f)

    #     final_fts = self.step4_dem_ft_names[feature_order]
        
    #     # Combine demography fts w/ main feature matrix
    #     train_df = pd.DataFrame(np.array(X_shuftrainSys_captum), columns=final_fts[:n_dims])
    #     test_df = pd.concat((test_data['demography_train'], test_data['features_train']), axis=1)
    #     test_df = test_df.loc[:, final_fts[:n_dims]].reset_index(drop=True)
    #     assert test_df.columns.equals(train_df.columns)

    #     test_df.fillna(train_df.mean(), inplace=True)
    #     assert test_df.isnull().any().sum() == 0

    #     gt_test = test_data['ground_truth']
    #     assert gt_test.isnull().any().sum() == 0

    #     print("Model Training: X_test shape: {}".format(test_df.shape))
    #     self.X_test, self.y_test = pd.DataFrame(test_df.values), pd.DataFrame(gt_test.values)
    #     X_test, y_test = pd.DataFrame(test_df.values), pd.DataFrame(gt_test.values)

    #     # Transform features to scalars and then predict
    #     X1 = scaler1.transform(X_test)
    #     X1 = torch.from_numpy(X1).type(torch.FloatTensor).to(device)

    #     X2 = scaler2.transform(X_test)
    #     X2 = torch.from_numpy(X2).type(torch.FloatTensor).to(device)

    #     X3 = scaler3.transform(X_test)
    #     X3 = torch.from_numpy(X3).type(torch.FloatTensor).to(device)

    #     X4 = scaler4.transform(X_test)
    #     X4 = torch.from_numpy(X4).type(torch.FloatTensor).to(device)

    #     X5 = scaler5.transform(X_test)
    #     X5 = torch.from_numpy(X5).type(torch.FloatTensor).to(device)

    #     # Since we have 5 models coming out of 5-fold CV, we average their predictions.
    #     # Get xgb predictions trained on Raw ground truths
    #     print("Model Training: Averaging XGB model predictions...")

    #     xgb_preds_av = np.mean([
    #         net_CV1(X1).detach().cpu().numpy().flatten(), 
    #         net_CV2(X2).detach().cpu().numpy().flatten(), 
    #         net_CV3(X3).detach().cpu().numpy().flatten(),
    #         net_CV4(X4).detach().cpu().numpy().flatten(), 
    #         net_CV5(X5).detach().cpu().numpy().flatten()
    #     ], axis=0)

    #     xgb_preds_df = pd.DataFrame()
    #     xgb_preds_df[0] = xgb_preds_av
    #     self.xgb_preds_df = xgb_preds_df
    #     print("Model Training: XGB model predictions averaged")
    #     print()

    #     print("Model Training Results ({}):".format(self.target))
    #     ErrpristSys = xgb_preds_df[0] - y_test[1]
    #     print('Bias: ', ErrpristSys.mean())
    #     print('SDError: ', ErrpristSys.std())
    #     print('RMSE: ', mean_squared_error(y_test[1], xgb_preds_df[0], squared=False))
    #     print()

    #     # Compression Index: SDpred/SDGT
    #     print("SD ratio of prediction over ground truth: ")
    #     print ("Systolic BP is {}".format(xgb_preds_df[0].std()/y_test[1].std()))
    #     print()
    #     print("Correlation between ground truth and prediction: ")
    #     print ("Systolic BP is {}".format(stats.pearsonr(y_test[1], xgb_preds_df[0])))
    #     print()

    #     # SDerror/SDGT - Uncertainty remaining
    #     print("SD error of prediction over ground truth: ")
    #     print ("Systolic BP is {}".format(ErrpristSys.std()/y_test[1].std()))

# class NuraModel(nn.Module):
#     def __init__(self, input_shape):
#         super().__init__()
        
#         # Define architecture
#         self.hidden1 = nn.Linear(input_shape, 256)
#         self.relu1 = nn.ReLU()
#         self.hidden2 = nn.Linear(256, 256)
#         self.relu2 = nn.ReLU()
#         self.output_layer = nn.Linear(256, 1)

#         # Initialize layer weights for reproducibility
#         nn.init.xavier_uniform_(self.hidden1.weight)
#         nn.init.zeros_(self.hidden1.bias)
#         nn.init.xavier_uniform_(self.hidden2.weight)
#         nn.init.zeros_(self.hidden2.bias)
#         nn.init.xavier_uniform_(self.output_layer.weight)
#         nn.init.zeros_(self.output_layer.bias)

#         # self.hidden_drop1 = nn.Dropout(0.2)
#         # self.hidden_drop2 = nn.Dropout(0.2)
#         # self.hidden_drop3 = nn.Dropout(0.2)
#         # self.hidden_drop4 = nn.Dropout(0.2)
#         # self.hidden_drop5 = nn.Dropout(0.2)

#     def forward(self, x):
#         x = self.relu1(self.hidden1(x))
#         # x = self.hidden_drop1(x)
#         x = self.relu2(self.hidden2(x))
#         # x = self.hidden_drop2(x)
#         x = self.output_layer(x)
#         return x

# class Nura_Dataset(Dataset):
#     def __init__(self, X, Y):
#         self.X = X
#         self.y = Y
#     def __len__(self):
#         return len(self.y)
#     def __getitem__(self, idx):
#         return self.X[idx], self.y[idx]



