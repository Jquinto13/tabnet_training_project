# TabNet Training and Evalutation - John Quinto #
* Performs model training on BP data using the TabNet architecture (https://arxiv.org/pdf/1908.07442v5.pdf)
* Implementation taken from https://github.com/dreamquark-ai/tabnet

### How do I get set up? ###
* TabNet: pip install pytorch-tabnet
* Requires several data files from /data/intern/john/PKL/:
	* '/data/intern/john/PKL/test_data_loading32_select_shap.pkl': contains testing data
	* '/data/intern/john/PKL/step4_fts.pkl': contains 1762 features following correlation filtering
	* '/data/intern/john/AD_Elite_GT.list': contains 554 Alan-selected features (we usually use the first 350)
* Run tabnet_train.py

### Issues ###
- Performs slightly worse than current DNN models
- May be some issues in the average ensembling/versions that lead to this discrepancy
