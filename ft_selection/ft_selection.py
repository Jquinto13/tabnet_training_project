import numpy as np
import pandas as pd
import scipy.stats as stats
import time
import pickle
from xgboost import XGBRegressor

from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn import preprocessing

class Feature_Selection:
    """
    Whole pipeline can either use Pandas, or utilize separate ft_names and ft_matrix properties
    """
    def __init__(self, pkl_path, target, float_type='float64'):
        self.target_dict = {"diastolic": 0, "systolic": 1, "pp": 2}
        if target not in self.target_dict.keys():
            print("Target must be one of {'diastolic','systolic','pp'}")
            return
        self.target = target
        self.finished_ops = {
            'nan_fts': False, 
            'sparse_fts': False, 
            'redundant_fts': False, 
            'demography_fts': False, 
            'ft_ranking': False
            }
        self.step2_ft_names, self.step2_ft_matrix = None, None
        self.step3_ft_names, self.step3_ft_matrix = None, None
        self.step4_ft_names, self.step4_ft_matrix = None, None
        self.step4_dem_ft_names, self.step4_dem_ft_matrix = None, None
        self.step5_ft_names, self.step5_ft_matrix = None, None
        self.load_data(pkl_path, target, float_type)
    
    def load_data(self, pkl_path, target, float_type):
        """
        New way of loading data that doesn't require any matlab containers
        Assumes we have 3 dataframes (features, ground_truth, and demographic features) in a .pkl file generated via 'data_loading.py'
        """
        print("Step 1: Loading .pkl file...")
        with open(pkl_path, 'rb') as f:
            # Call load method to deserialze
            data_dict = pickle.load(f)
        print("Step 1: .pkl file loaded")

        features_df = data_dict['features_train']
        gt_df = data_dict['ground_truth']
        dem_df = data_dict['demography_train']

        self.ft_names = features_df.columns.to_list()
        self.ft_names_demography = dem_df.columns.to_list()
        self.subject_IDs = features_df.index.to_list()

        self.ft_matrix_demography = dem_df.values
        self.ft_matrix = features_df.values
        self.full_gt_data = gt_df.values

        # Option to set float type
        if float_type != 'float64':
            self.ft_matrix = self.ft_matrix.astype(float_type)
            self.ft_matrix_demography = self.ft_matrix_demography.astype(float_type)

        # If ground-truth data contains NaNs, then filter those rows/subjects out
        self.gt_target = self.full_gt_data[:, self.target_dict[target]] # Select ground truth columnn (sys, dias, pp...)
        self.gt_target = self.gt_target.astype(float_type)
        gt_mask = ~np.isnan(self.gt_target)

        self.ft_matrix = self.ft_matrix[gt_mask, :] # filter feature matrix 
        self.ft_matrix_demography = self.ft_matrix_demography[gt_mask, :]  # filter demography matrix
        self.full_gt_data = self.full_gt_data[gt_mask, :] # filter ground-truth matrix
        self.gt_target = self.gt_target[gt_mask]
        self.subject_IDs = np.array(self.subject_IDs)[gt_mask]

        self.df = pd.DataFrame(self.ft_matrix, columns=self.ft_names, index=self.subject_IDs)
        self.step1_ft_names = self.ft_names
        self.step1_ft_matrix = self.ft_matrix
        print("Step 1: Scheme02 features: {}".format(self.df.shape))
        print()

    def get_feats_nan_ids(self, nan_thresh=0.015):
        """
        nan_thresh must be from 0-1, inclusive
        """
        if self.finished_ops['nan_fts'] is True:
            print("NaN features already removed")
            return
        # df.replace([np.inf, -np.inf], np.nan, inplace=True) # Replace inf values w/ NaNs
        print("Step 2: Removing NaN features...")
        print("Step 2: NaN threshold = {}".format(nan_thresh))

        min_count = int((1 - nan_thresh)*self.df.shape[0] + 1)
        self.df.dropna(thresh=min_count, axis=1, inplace=True)
        self.ft_matrix =  np.array(self.df)
        self.ft_names = np.array(self.df.columns.to_list())
        self.finished_ops['nan_fts'] = True

        print("Step 2: NaN features removed")
        print("Step 2: Remaining features = {}".format(self.df.shape))
        print()
        self.step2_ft_names = self.ft_names
        self.step2_ft_matrix = self.ft_matrix

        # # Further step: remove subjects that have a certain percentage of NaN features
        # min_subject_count = int((1 - self.nan_thresh)*self.df.shape[1] + 1)
        # self.df.dropna(thresh=min_subject_count, axis=0, inplace=True)

    def get_feats_sparse_and_no_var_ids(self, sparse_thresh=0):
        """
        sparse_thresh must be between 0-1, inclusive
        """
        if self.finished_ops['sparse_fts'] is True:
            print("Sparse features already removed")
            return
        print("Step 3: Removing sparse features...")
        print("Step 3: Sparsity threshold = {}".format(sparse_thresh))
        
        # Impute NaNs in each column with the mean of that column (we might consider kNN imputation in the future)
        self.df.fillna(self.df.mean(), inplace=True)

        # Compute Z-Score of each column; if that column has a Z-score sum of 0 or NaN, then remove it
        z_scores = stats.zscore(self.df, ddof=1)
        zdf = pd.DataFrame(z_scores, columns=self.df.columns)
        zdf.drop([col for col, val in zdf.sum().iteritems() if val == sparse_thresh or np.isnan(val)], axis=1, inplace=True) ## == sparse_thresh or > abs(sparse_thresh)?

        self.df = self.df.loc[:, zdf.columns.to_list()]
        self.ft_matrix =  np.array(self.df)
        self.ft_names = np.array(self.df.columns.to_list())
        self.finished_ops['sparse_fts'] = True

        print("Step 3: Sparse features removed")
        print("Step 3: Remaining features = {}".format(self.df.shape))
        print()
        self.step3_ft_names = self.ft_names
        self.step3_ft_matrix = self.ft_matrix

    def corr(self, a, b):
        """
        Python version of Matlab's corr() function 
        """
        a = (a - a.mean(axis=0))/a.std(axis=0)
        b = (b - b.mean(axis=0))/b.std(axis=0)
        corr_coef = np.dot(b.T, a)/b.shape[0]
        if isinstance(corr_coef, np.ndarray):
            return corr_coef[0]
        return corr_coef       

    def remove_redundant_feats(self, corr_thresh=0.7, min_feats_thresh=600): 
        """
        Ported directly from Matlab (~23-27 min)
        """     
        start_time = time.time() 
        if self.finished_ops['redundant_fts'] is True:
            print("Redundant features already removed")
            return

        if np.count_nonzero(np.isnan(self.gt_target)) > 0:
            print("Ground truth data contains NaN values")
            return

        print("Step 4: Removing redundant features...")
        print("Step 4: Correlation threshold = {}".format(corr_thresh))
        print("Step 4: Minimum features threshold = {}".format(min_feats_thresh))

        # Get column-wise correlation 
        corrFeat = self.corr(self.ft_matrix, self.gt_target.reshape(-1, 1))
        absCorrFeat = abs(corrFeat)

        # Sort correlation matrix in descending order
        sortAbsCorrFeat, idxSortFeat = np.sort(absCorrFeat)[::-1], np.argsort(absCorrFeat)[::-1]
        # Sort feature matrix columns in same order as above
        self.ft_matrix = self.ft_matrix[:, idxSortFeat]
        # Sort feature names by same order as above
        self.ft_names = self.ft_names[idxSortFeat]

        idx = list(range(self.ft_matrix.shape[1]))

        for k in range(self.ft_matrix.shape[1]):
            if k % 100 == 0:
                print("k={} ; {} ".format(k, np.array(idx).shape))
            idx_rem = []
            for i in range(k+1, len(idx)):
                if abs(self.corr(self.ft_matrix[:, idx[k]], self.ft_matrix[:, idx[i]])) > corr_thresh: # Could we vectorize this?
                    idx_rem.append(i)
            idx = np.delete(idx, idx_rem)
            if len(idx) <= min_feats_thresh or k > len(idx):
                break
        feat_rem_index = list(idx)
        self.ft_matrix = self.ft_matrix[:, feat_rem_index]
        self.ft_names = self.ft_names[feat_rem_index]
        self.df = pd.DataFrame(self.ft_matrix, columns=self.ft_names, index=self.subject_IDs)
        # self.df = self.df.loc[:, feat_rem_index]
        self.finished_ops['redundant_fts'] = True
        print("Step 4: Redundant features removed")
        print("Step 4: Remaining features = {}".format(self.df.shape))
        print("Step 4: Elapsed time: {} min".format((time.time()-start_time)/60))
        print()
        self.step4_ft_names = self.ft_names
        self.step4_ft_matrix = self.ft_matrix
    
    def remove_redundant_feats_alt(self, corr_thresh=0.7):
        def determine_high_cor_pair(correlation_row, sorted_correlation_pairs):
            """
            Select highest correlated variable given a correlation row with columns:
            ["pair_a", "pair_b", "correlation"]
            For use in a pandas.apply()
            """

            pair_a = correlation_row["pair_a"]
            pair_b = correlation_row["pair_b"]

            if sorted_correlation_pairs.get_loc(pair_a) > sorted_correlation_pairs.get_loc(
                pair_b
            ):
                return pair_a
            else:
                return pair_b

        start_time = time.time() 

        if self.finished_ops['redundant_fts'] is True:
            print("Redundant features already removed")
            return
        if np.count_nonzero(np.isnan(self.gt_target)) > 0:
            print("Ground truth data contains NaN values")
            return

        print("Step 4 ALT: Removing redundant features...")
        print("Step 4 ALT: Correlation threshold = {}".format(corr_thresh))

        # Get column-wise correlation 
        corrFeat = self.corr(self.ft_matrix, self.gt_target.reshape(-1, 1))
        absCorrFeat = abs(corrFeat)

        # Sort correlation matrix in descending order
        sortAbsCorrFeat, idxSortFeat = np.sort(absCorrFeat)[::-1], np.argsort(absCorrFeat)[::-1]
        # Sort feature matrix columns in same order as above
        self.ft_matrix = self.ft_matrix[:, idxSortFeat]
        # Sort feature names by same order as above
        self.ft_names = self.ft_names[idxSortFeat]

        data_cor = np.corrcoef(self.ft_matrix, rowvar=False)
        data_cor_df = pd.DataFrame(data_cor, columns=self.ft_names, index=self.ft_names)

        # Create a copy of the dataframe to generate upper triangle of zeros
        data_cor_natri_df = data_cor_df.copy()

        # Replace upper triangle in correlation matrix with NaN
        data_cor_natri_df = data_cor_natri_df.where(
            np.tril(np.ones(data_cor_natri_df.shape), k=-1).astype(np.bool)
        )

        # Acquire pairwise correlations in a long format
        # Note that we are using the NaN upper triangle DataFrame
        pairwise = data_cor_natri_df.stack().reset_index()
        pairwise.columns = ["pair_a", "pair_b", "correlation"]

        # Get absolute sum of correlation across features
        # The lower the index, the less correlation to the full data frame
        # We want to drop features with highest correlation, so drop higher index
        variable_cor_sum = data_cor_df.abs().sum().sort_values().index

        # And subset to only variable combinations that pass the threshold
        threshold = corr_thresh
        pairwise = pairwise.query("correlation > @threshold")

        # Output the excluded features
        excluded = pairwise.apply(
            lambda x: determine_high_cor_pair(x, variable_cor_sum), axis="columns"
        )
        self.df = self.df.drop(list(set(excluded)), axis=1)
        self.ft_matrix = self.df.values
        self.ft_names = np.array(self.df.columns.to_list())

        self.finished_ops['redundant_fts'] = True
        print("Step 4 ALT: Redundant features removed")
        print("Step 4 ALT: Remaining features = {}".format(self.df.shape))
        print("Step 4 ALT: Elapsed time: {} min".format((time.time()-start_time)/60))
        print()
        
        self.step4_ft_names = self.ft_names
        self.step4_ft_matrix = self.ft_matrix

    def append_demographic_features(self):
        if self.finished_ops['demography_fts'] is True:
            print("Demography features already added")
            return

        print("Step 4_Dem: Appending {} demographic features... ".format(self.ft_matrix_demography.shape[1]))
        self.ft_names = np.concatenate((self.ft_names_demography, self.ft_names), axis=0)
        self.ft_matrix = np.concatenate((self.ft_matrix_demography, self.ft_matrix), axis=1)
        self.df = pd.DataFrame(self.ft_matrix, columns=self.ft_names, index=self.subject_IDs)
        self.finished_ops['demography_fts'] = True
        print("Step 4_Dem: Remaining Features = {}".format(self.ft_matrix.shape))
        print()
        self.step4_dem_ft_names = self.ft_names
        self.step4_dem_ft_matrix = self.ft_matrix

    def get_RF_model(self, X1, Y1, n_est, n_jobs):
        """
        Taken directly from Naresh's 'rand_forests.ipynb'
        Trains a RandomForest model.
        100 is the defaults for n_estimators. 
        But for feature selection we need to exhaust the space as much as possible.
        """
        start_time = time.time()
        rf_CV = RandomForestRegressor(n_estimators=n_est, n_jobs=n_jobs)
        
        # Ensure all features are normalized (scaled between 0 and 1)
        scaler = preprocessing.MinMaxScaler()
        X_CV = scaler.fit_transform(X1)
        
        # Now, train the 5 models with all the predictors only using the training set splits
        rf_CV.fit(X_CV, Y1[self.target_dict[self.target]])
        
        # First, we can check to see how much of the variance in the training data is captured by the model.
        # We can compute R-squared for this (also referred to as the coefficient of determination).
        print("Step 5: R-squared for each subsample's training data: ", rf_CV.score(X_CV, Y1[1]))
        print("Step 5: Mean squared error on the training set: ", mean_squared_error(Y1[1], rf_CV.predict(X_CV)))
        print("Step 5: Elapsed time: {} min".format((time.time()-start_time)/60))
        
        return X_CV, Y1, rf_CV, scaler
        
    def get_feature_importances(self, save_path, n_est=10000, n_jobs=12):
        if self.finished_ops['ft_ranking'] is True or self.finished_ops['demography_fts'] is False:
            print("Feature ranking already complete or demography features missing")
            return
        print("Step 5: Obtaining feature importances for target '{}''...".format(self.target))
        # Get features and ground truths
        Y_trainpre = self.full_gt_data
        X_trainSys = self.ft_matrix
        
        # Convert to dataframes
        X_trainSys = pd.DataFrame(X_trainSys)
        Y_train = pd.DataFrame(Y_trainpre)

        # Shuffle X and Y
        np.random.seed(0)
        shufId = np.random.permutation(int(len(Y_train)))
        X_shuftrainSys = X_trainSys.iloc[shufId]
        Y_shuftrain = Y_train.iloc[shufId]
        
        X_shuftrainSys = X_shuftrainSys.reset_index(drop=True)
        Y_shuftrain = Y_shuftrain.reset_index(drop=True)
        
        # Run random forests feature selection 
        X_CV1Sys1, Y_CV1, rfSys1CV1, scaler1Sys1 = self.get_RF_model(X_shuftrainSys, Y_shuftrain, n_est, n_jobs)
        print("Step 5: Feature importances calculated")

        rfSysImp1 = pd.DataFrame(rfSys1CV1.feature_importances_)
        colSys1 = rfSysImp1.sort_values(by=[0], ascending=False)

        colSys = pd.DataFrame()
        colSys[0] = colSys1.index
        self.ft_rankings = list(colSys1.index)

        self.ft_matrix = self.ft_matrix[:, self.ft_rankings]
        self.ft_names = self.ft_names[self.ft_rankings]
        self.df = pd.DataFrame(self.ft_matrix, columns=self.ft_names, index=self.subject_IDs)

        # Give a file name that identifies at least the target (i.e. Systolic in this case), and the date
        print("Step 5: Saving feature importances to {}...".format(save_path))
        colSys.to_csv(save_path)
        print("Step 5: Feature importances saved to {}...".format(save_path))
        self.finished_ops['ft_ranking'] = True
        print()
        self.step5_ft_matrix = self.ft_matrix
        self.step5_ft_names = self.ft_names

    def get_XGB_fold_Model(self, X, Y1, foldsize, foldnum, n_estimators, max_depth, n_jobs):
        start_time = time.time()
        nExtra = 1
        if foldnum == 5:
            if self.target == 'diastolic':
                X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
            elif self.target == 'systolic':
                X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
            elif self.target == 'pp':
                X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
            Y_CV = Y1.drop(Y1[(foldnum-1)*foldsize:(foldnum)*foldsize+nExtra].index)
        else:
            if self.target == 'diastolic':
                X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
            elif self.target == 'systolic':
                X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
            elif self.target == 'pp':
                X_CV = X.drop(X[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
            Y_CV = Y1.drop(Y1[(foldnum-1)*foldsize:(foldnum)*foldsize].index)
        
        # Create instances of the RandomForest model.        
        xgb_CV = XGBRegressor(n_estimators=n_estimators, max_depth=max_depth, n_jobs=n_jobs)
        
        # Ensure all features are normalized (scaled between 0 and 1)
        scaler = preprocessing.MinMaxScaler()
        X_CV_scaled = scaler.fit_transform(X_CV)
        xgb_CV.fit(X_CV_scaled, Y_CV[1])
        
        # First, we can check to see how much of the variance in the training data is captured by the model.
        # We can compute R-squared for this (also referred to as the coefficient of determination).
        print("Model Training: R-squared for fold {} training data: {}".format(foldnum, xgb_CV.score(X_CV_scaled, Y_CV[1])))
        print("Model Training: Mean squared error on fold {} training data: {}".format(foldnum, mean_squared_error(Y_CV[1], xgb_CV.predict(X_CV_scaled))))
        print("Model Training: Elapsed time: {} sec".format(time.time()-start_time))
        print()
        
        return X_CV_scaled, Y_CV, xgb_CV, scaler
    
    def load_test_data(self, test_path, n_dims):
        print("Model Training: Loading test data...")
        with open(test_path, 'rb') as f:
            test_data = pickle.load(f)
        print("Model Training: Test data loaded")
        print("Model Training: Test data shape {}".format(test_data['features_train'].shape))

        # Load and process test features
        test_df = pd.concat((test_data['demography_train'], test_data['features_train']), axis=1)
        print("Model Training: Filtering test data using selected features...")
        test_df = test_df.loc[:, self.ft_names]
        test_df = test_df.iloc[:, :n_dims]
        print("Model Training: Filtered test data shape {}".format(test_df.shape))
        print("Model Training: Filling {} NaNs w/ mean of training data...".format(test_df.isnull().any().sum()))
        test_df.fillna(self.df.mean(), inplace=True)
        print("Model Training: Nulls remaining: {}".format(test_df.isnull().any().sum()))

        # Load and process test ground truth data
        gt_data = test_data['ground_truth']
        print("Model Training: Ground truth data shape: {}".format(gt_data.shape))
        print("Model Training: Number of ground truth data NaNs: {}".format(gt_data.isnull().any().sum()))

        X_test = pd.DataFrame(test_df.values)
        X_test = X_test.T.reset_index(drop=True).T
        y_test = pd.DataFrame(gt_data.values)

        return X_test, y_test

    def train_XGB_model(self, test_path, n_dims=100, n_estimators=300, max_depth=4, n_jobs=-1):
        # if ~np.all(self.finished_ops.values()):
        #     print("Feature selection operations not complete")
        #     return
        # Load in X_train and y_train
        X_train = self.df
        Y_train = pd.DataFrame(self.full_gt_data)

        print("Model Training: Fitting XGB regressors for target '{}''...".format(self.target))
        print('Model Training: X_train shape: {}'.format(X_train.shape))
        print('Model Training: y_train shape: {}'.format(Y_train.shape))

        # Reduce to top 100 features - Alan's findings from performance curves
        print("Model Training: Selecting top {} features...".format(n_dims))
        X_train = X_train.iloc[:, :n_dims]
        print("Model Training: X_train shape after selecting top {} features: {}".format(n_dims, X_train.shape))

        # Shuffle the training set. But seed random order for replicability
        print("Model Training: Shuffling training set...")
        np.random.seed(0)
        shufId = np.random.permutation(int(len(Y_train)))
        X_shuftrainSys = X_train.iloc[shufId]
        Y_shuftrain = Y_train.iloc[shufId]
        X_shuftrainSys = X_shuftrainSys.reset_index(drop=True)
        Y_shuftrain = Y_shuftrain.reset_index(drop=True)
        print("Model Training: Training set shuffled")
        print()

        # This part is just to distribute across 5 folds. Can be changed.
        # Right now I am not separating by participant. Only separating by measurement.
        # A cleaner, no contamination shuffle will involve separating by participant.
        nParts = X_shuftrainSys.shape[0] # K-Fold using sklearn?
        print("Model Training: Training XGB models...")
        X_CV1, Y_CV1, xgb_CV1, scaler1 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),1, n_estimators, max_depth, n_jobs)
        X_CV2, Y_CV2, xgb_CV2, scaler2 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),2, n_estimators, max_depth, n_jobs)
        X_CV3, Y_CV3, xgb_CV3, scaler3 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),3, n_estimators, max_depth, n_jobs)
        X_CV4, Y_CV4, xgb_CV4, scaler4 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),4, n_estimators, max_depth, n_jobs)
        X_CV5, Y_CV5, xgb_CV5, scaler5 = self.get_XGB_fold_Model(X_shuftrainSys,Y_shuftrain,int(nParts/5),5, n_estimators, max_depth, n_jobs)
        print("Model Training: XGB models trained")
        X_test, y_test = self.load_test_data(test_path, n_dims)
        self.X_test, self.y_test = X_test, y_test

        # Transform features to scalars and then predict
        X1 = scaler1.transform(X_test)
        X2 = scaler2.transform(X_test)
        X3 = scaler3.transform(X_test)
        X4 = scaler4.transform(X_test)
        X5 = scaler5.transform(X_test)

        # Since we have 5 models coming out of 5-fold CV, we average their predictions.
        # Get xgb predictions trained on Raw ground truths
        print("Model Training: Averaging XGB model predictions...")
        xgb_preds_av = np.mean([
            xgb_CV1.predict(X1), 
            xgb_CV2.predict(X2), 
            xgb_CV3.predict(X3), 
            xgb_CV4.predict(X4), 
            xgb_CV5.predict(X5)
            ], axis=0)
        xgb_preds_df = pd.DataFrame()
        xgb_preds_df[0] = xgb_preds_av
        self.xgb_preds_df = xgb_preds_df
        print("Model Training: XGB model predictions averaged")
        print()

        print("Model Training Results ({}):".format(self.target))
        ErrpristSys = xgb_preds_df[0] - y_test[1]
        print('Bias: ', ErrpristSys.mean())
        print('SDError: ', ErrpristSys.std())
        print('RMSE: ', mean_squared_error(y_test[1], xgreconstrub_preds_df[0], squared=False))
        print()
        # Compression Index: SDpred/SDGT
        print("SD ratio of prediction over ground truth: ")
        print ("Systolic BP is {}".format(xgb_preds_df[0].std()/y_test[1].std()))
        print()
        print("Correlation between ground truth and prediction: ")
        print ("Systolic BP is {}".format(stats.pearsonr(y_test[1], xgb_preds_df[0])))
        print()
        # SDerror/SDGT - Uncertainty remaining
        print("SD error of prediction over ground truth: ")
        print ("Systolic BP is {}".format(ErrpristSys.std()/y_test[1].std()))
        
                
    def perform_all_FS_ops(self, save_path, test_path, nan_thresh=0.015, sparse_thresh=0, corr_thresh=0.7, min_feats_thresh=600, nest=10000):
        """
        Performs all feature selection operations
        """
        self.get_feats_nan_ids(nan_thresh)
        self.get_feats_sparse_and_no_var_ids(sparse_thresh)
        self.remove_redundant_feats(corr_thresh, min_feats_thresh)
        self.append_demographic_features()
        self.get_feature_importances(save_path, nest)
        self.train_XGB_model(test_path)